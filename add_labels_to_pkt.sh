#How to use:
#./add_labels_to_pkt.sh [Day]
#Example:
#./add_labels_to_pkt.sh Tuesday

#Arborescence:
#.
#├── add_labels_to_pkt.sh
#├── labelled_flows ==> folder containing all pcap_Flow files labelled
#│   ├── Monday-WorkingHours.pcap_Flow_Labelled.csv
#│   ├── Tuesday-WorkingHours.pcap_Flow_Labelled.csv
#│   ├── Wednesday-WorkingHours.pcap_Flow_Labelled.csv
#│   ├── Thursday-WorkingHours.pcap_Flow_Labelled.csv
#│   ├── Friday-WorkingHours.pcap_Flow_Labelled.csv
#├── pkt ==> folder containing all pcap_pktlist files
#│   ├── Monday-WorkingHours.pcap_pktlist.csv
#│   ├── Tuesday-WorkingHours.pcap_pktlist.csv
#│   ├── Wednesday-WorkingHours.pcap_pktlist.csv 
#│   ├── Thursday-WorkingHours.pcap_pktlist.csv
#│   ├── Friday-WorkingHours.pcap_pktlist.csv
#├── labelled_pkt ==> folder containing all pcap_pktlist files labelled

#Columns' numbers
#$1=Flow ID
#$7=EpochTime
#$84=Label

#Create work directory if it does not exist
mkdir -p join

#Create and prepare files:

#labelled_flows
echo "Preparing flow file for $1"
awk -F, '{printf ("%s-%d,%s\n", $1, $7, $84)}' "labelled_flows/$1-WorkingHours.pcap_Flow_Labelled.csv" > "join/$1_flow_labelled.csv"
#Remove first line (column names)
sed -i 1d "join/$1_flow_labelled.csv"

#pktlist
echo "Preparing pktlist file for $1"
#Remove first line + remove 'NeedManualLabel' + Remove last coma + add 'Flow ID-EpochTime' as first column
sed 1d "pkt/$1-WorkingHours.pcap_pktlist.csv" | awk -F',' 'BEGIN{OFS=","} {$NF=""}1' | sed 's/,$//' | awk -F, '{printf ("%s-%d,", $1, $7); print $0;}' > "join/$1_pktlist.csv"
#Join the two files and remove duplicates
echo "Joining the two files ..."
join -t , -1 1 -2 1 <(sort -t , -k 1,1 "join/$1_pktlist.csv") <(sort -t , -k 1,1 "join/$1_flow_labelled.csv") | sort -u -o "join/$1_pktlist_Labelled.csv"
#Remove first column and store in 'labelled_pkt'
echo "Storing labelled pktlist file for $1"
sed 's/[^,]*,//' "join/$1_pktlist_Labelled.csv" > "labelled_pkt/$1-WorkingHours.pcap_pktlist_Labelled.csv"
echo "Finished labelling $1-WorkingHours.pcap_pktlist.csv"
