"""
Filename: labelling_by_epoch_time_match.py
Date: Dec 15, 2020 - 11:45
Name: Rami MARZOUGUI, Khaled ETTRIKI
Description:
    - Labelling the flows of the generated CSV files using the epoch time approach
"""

# Arborescence necessaire :
# ├── AddEpochTime.py
# ├── CFM
# │         └── csv_files
# │             ├── Friday-WorkingHours.pcap_Flow.csv
# │             ├── Monday-WorkingHours.pcap_Flow.csv
# │             ├── Thursday-WorkingHours.pcap_Flow.csv
# │             ├── Tuesday-WorkingHours.pcap_Flow.csv
# │             └── Wednesday-WorkingHours.pcap_Flow.csv
# ├── cicids2017
# │         └── csv_files
# │             ├── epoch
# │             │         ├── Friday-WorkingHours-Afternoon-DDos.pcap_ISCX_epoch.csv
# │             │         ├── Friday-WorkingHours-Afternoon-PortScan.pcap_ISCX_epoch.csv
# │             │         ├── Friday-WorkingHours-Morning.pcap_ISCX_epoch.csv
# │             │         ├── Monday-WorkingHours.pcap_ISCX_epoch.csv
# │             │         ├── Thursday-WorkingHours-Afternoon-Infilteration.pcap_ISCX_epoch.csv
# │             │         ├── Thursday-WorkingHours-Morning-WebAttacks.pcap_ISCX_epoch.csv
# │             │         ├── Tuesday-WorkingHours.pcap_ISCX_epoch.csv
# │             │         └── Wednesday-workingHours.pcap_ISCX_epoch.csv
# │             ├── Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv
# │             ├── Friday-WorkingHours-Afternoon-PortScan.pcap_ISCX.csv
# │             ├── Friday-WorkingHours-Morning.pcap_ISCX.csv
# │             ├── Monday-WorkingHours.pcap_ISCX.csv
# │             ├── Thursday-WorkingHours-Afternoon-Infilteration.pcap_ISCX.csv
# │             ├── Thursday-WorkingHours-Morning-WebAttacks.pcap_ISCX.csv
# │             ├── Tuesday-WorkingHours.pcap_ISCX.csv
# │             └── Wednesday-workingHours.pcap_ISCX.csv
# ├── labelling_by_epoch_time_match.py
# ├── output
# │         └── labelled_flows
# ├── python_logs

from datetime import datetime
import pandas as pd
import logging
import os
import glob
import multiprocessing as mp
import time

# CONSTANTS
LOG_PATH = "./python_logs/"
INPUT_PATH_ISCX = "./cicids2017/csv_files/epoch/"
INPUT_PATH_CFM = "./CFM/csv_files/"
OUTPUT_PATH_LABELLED = "./output/labelled_flows/"
LABEL_NAME_ISCX = ' Timestamp'
LABEL_NAME_CFM = 'EpochTime'
EXT = ".csv"
EPOCH_EXT = "_epoch.csv"

CSV1_FILELIST = [
    "Monday-WorkingHours.pcap_Flow",
    "Tuesday-WorkingHours.pcap_Flow",
    "Wednesday-WorkingHours.pcap_Flow",
    "Thursday-WorkingHours.pcap_Flow",
    "Friday-WorkingHours.pcap_Flow"
]


def initialize_logger(output_dir):
    """
    Initialize logger so that errors are logged in error.log, all messages
    are logged in all.log printed in console

    Parameters
    ----------
    output_dir: str
        String corresponding to the path of the log file

    Returns
    -------
    None
    """
    pyfile = os.path.splitext(os.path.split(__file__)[1])[0]
    now = datetime.now().strftime("%Y%m%d_%H%M%S")
    filename = pyfile + "_info_" + now + ".log"
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    # create console handler and set level to info
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter("[%(levelname)s] %(asctime)s - "
                                  "%(message)s", datefmt="%Y-%m-%d %H:%M:%S")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    # create debug file handler and set level to debug
    handler = logging.FileHandler(os.path.join(output_dir, filename), "w")
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("[%(levelname)s] %(asctime)s - "
                                  "%(message)s", datefmt="%Y-%m-%d %H:%M:%S")
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def read_csv(filename):
    """
    load csv file into a DataFrame

    Parameters
    ----------
    filename: str
        file name of the CSV that is loaded

    Returns
    -------
    df: DataFrame
        Pandas DataFrame containing data from CSV
    """
    process_name = mp.current_process().name
    pid = os.getpid()
    template = "{} (PID {}) - Loading CSV {}"
    logging.info(template.format(process_name, pid, filename))
    df = pd.read_csv(filename, encoding="ISO-8859-1", low_memory=False)
    template = "{} (PID {}) - Loaded CSV {}"
    logging.info(template.format(process_name, pid, filename))
    return df


def write_csv(df, filename):
    """
    Write content of DataFrame into a CSV file
    Parameters
    ----------
    df: DataFrame
        DataFrame containing data to be written in CSV
    filename: str
        File name of the CSV to write

    Returns
    -------
    None
    """
    process_name = mp.current_process().name
    pid = os.getpid()
    template = "{} (PID {}) - Writing CSV {}"
    logging.info(template.format(process_name, pid, filename))
    df.to_csv(filename, index=False)
    template = "{} (PID {}) - Written CSV {}"
    logging.info(template.format(process_name, pid, filename))


def merge_multiple_files(day):
    """
    Merge multiple CSV files of the same day and export as CSV.

    Parameters
    ----------
    day : str
        The day for which the CSV files to combine were generated

    Returns
    -------
    None
    """
    process_name = mp.current_process().name
    pid = os.getpid()
    template = "{} (PID {}) - Merging multiple files"
    logging.info(template.format(process_name, pid))
    all_files = glob.glob(os.path.join(INPUT_PATH_ISCX, day + "-*" + EPOCH_EXT))
    df_from_each_file = (pd.read_csv(f, sep=',') for f in all_files)
    df_merged = pd.concat(df_from_each_file, ignore_index=True)
    df_merged.to_csv(day + "-WorkingHours.pcap_ISCX" + EPOCH_EXT)
    template = "{} (PID {}) - Merged {} files together for {}"
    logging.info(template.format(process_name, pid, len(all_files), day))


def sort_by_epoch(df, column_name):
    """
    Sort a Pandas DataFrame by the column containing Epoch Time in an ascending order.
    The column name is passed as a parameter because it differs depending on the source
    csv file being original or generated.

    Parameters
    ----------
    df: DataFrame
        Pandas DataFrame to be sorted

    Returns
    -------
    None
    """
    process_name = mp.current_process().name
    pid = os.getpid()
    template = "{} (PID {}) - Sorting by epoch time"
    logging.info(template.format(process_name, pid))
    df.sort_values(by=[column_name], inplace=True, ascending=True)
    template = "{} (PID {}) - Sorted all lines"
    logging.info(template.format(process_name, pid))


def create_timestamp_table(df_sorted):
    """
    Create a Pandas DataFrame containing the timestamp and the corresponding first
    and last index in the sorted DataFrame of the original csv file.

    Parameters
    ----------
    df_sorted: DataFrame
        Sorted Pandas DataFrame used to create the timestamp indexes table

    Returns
    -------
    df_timestamp : DataFrame
        Pandas DataFrame containing the timestamp, first_index and last_index
    """
    process_name = mp.current_process().name
    pid = os.getpid()
    template = "{} (PID {}) - Creating timestamp table"
    logging.info(template.format(process_name, pid))
    df_timestamp = df_sorted.index.to_series().groupby(df_sorted[LABEL_NAME_ISCX]).agg(
        ['first', 'last']).reset_index()
    template = "{} (PID {}) - Timestamp table created"
    logging.info(template.format(process_name, pid))
    return df_timestamp


def get_indexes(timestamp, df_timestamp):
    """
    Return the first and last indexes corresponding to the timestamp in the timestamp table.

    Parameters
    ----------
    df_timestamp : DataFrame
        Pandas DataFrame containing the timestamp, first_index and last_index
    timestamp: integer
        An integer that represents the epoch time in seconds

    Returns
    -------
    indexes : Tuple
        Tuple containing the first and last index
    """

    n_rows = df_timestamp.shape[0]
    indexes = (-1, -1)
    for i in range(n_rows):
        if timestamp == df_timestamp[LABEL_NAME_ISCX][i]:
            indexes = (df_timestamp['first'][i], df_timestamp['last'][i])
            break
    return indexes


def is_corresponding_flows(current_csv1_flow, current_iscx_flow):
    """
        Function that compares two flows to check if they correspond or not

        Parameters
        ----------
        current_csv1_flow: DataFrame
            Pandas DataFrame that represents the current generated flow
        current_iscx_flow: DataFrame
            Pandas DataFrame that represents the current original flow

        Returns
        -------
        corresponds : Boolean
            Boolean that indicates if the two flows correspond or not
        """
    corresponds = False
    if(current_csv1_flow[1] == current_iscx_flow[1] and current_csv1_flow[2] == current_iscx_flow[2] and
            current_csv1_flow[3] == current_iscx_flow[3] and current_csv1_flow[4] == current_iscx_flow[4] and
            current_csv1_flow[5] == current_iscx_flow[5] and current_csv1_flow[9] == current_iscx_flow[8] and
            current_csv1_flow[10] == current_iscx_flow[9] and current_csv1_flow[11] == current_iscx_flow[10] and
            current_csv1_flow[42] == current_iscx_flow[41] and current_csv1_flow[86] == current_iscx_flow[85]):
        corresponds = True
    return corresponds


def labelling_func(filename):
    """
    Function that will process the labelling of one file

    Parameters
    ----------
    filename: str
        File name of the generated CSV file to label (written without the extension)

    Returns
    -------
    None
    """
    process_name = mp.current_process().name
    pid = os.getpid()
    template = "{} (PID {}) - Labelling the flows of the generated file"
    logging.info(template.format(process_name, pid))
    df_csv1 = read_csv(INPUT_PATH_CFM + filename + EXT)
    if filename == "Thursday-WorkingHours.pcap_Flow":
        merge_multiple_files("Thursday")
    elif filename == "Friday-WorkingHours.pcap_Flow":
        merge_multiple_files("Friday")
    elif filename == "Wednesday-WorkingHours.pcap_Flow":
        # The ISCX file is called Wednesday-workingHours... while the generated one is called Wednesday-WorkingHours...
        filename = "Wednesday-workingHours.pcap_Flow"
    df_iscx = read_csv(INPUT_PATH_ISCX + filename[:-4] + "ISCX" + EPOCH_EXT)
    sort_by_epoch(df_csv1, LABEL_NAME_CFM)
    sort_by_epoch(df_iscx, LABEL_NAME_ISCX)
    df_timestamp = create_timestamp_table(df_iscx)
    process_name = mp.current_process().name
    pid = os.getpid()
    n_rows = df_csv1.shape[0]
    period = int(n_rows / 10)
    template = "{} (PID {}) - Labelled {}/{}"
    for i in range(n_rows):
        current_timestamp = df_csv1[LABEL_NAME_CFM][i]
        (first_idx, last_idx) = get_indexes(current_timestamp, df_timestamp)
        n_labels = 0
        label = 'Undetermined'
        if first_idx != -1:
            for j in range(first_idx, last_idx + 1):
                if is_corresponding_flows(df_csv1.loc[i, :], df_iscx.loc[j, :]):
                    n_labels = n_labels + 1
                    if n_labels == 1:
                        # Label found
                        label = df_iscx[' Label'][j]
                    elif label != df_iscx[' Label'][j]:
                        # Multiple corresponding flows with different labels
                        label = 'Undetermined'
                        break
        df_csv1['Label'][i] = label
        if i % period == 0:
            logging.info(template.format(process_name, pid, i, n_rows))
    template = "{} (PID {}) - Labelled {}/{} - Labelling done"
    logging.info(template.format(process_name, pid, n_rows, n_rows))
    # Save the results
    write_csv(df_csv1, OUTPUT_PATH_LABELLED + filename + "_Labelled" + EXT)


def main():
    # create output directories if they don't exist yet
    if not os.path.exists(OUTPUT_PATH_LABELLED):
        os.makedirs(OUTPUT_PATH_LABELLED)
    if not os.path.exists(LOG_PATH):
        os.makedirs(LOG_PATH)
    initialize_logger(LOG_PATH)
    # show # of CPUs and files
    n_cpu = mp.cpu_count()
    n_files = len(CSV1_FILELIST)
    template = "Number of CPUs: {} - Number of files: {}"
    logging.info(template.format(n_cpu, n_files))
    time.sleep(1)
    # select number of thread
    n_threads = 0
    max_thread = min(n_cpu, n_files)
    loop_exit = False
    while not loop_exit:
        template = "Enter # of processing thread (1-{}): ".format(max_thread)
        n_threads = int(input(template))
        if 0 < n_threads <= max_thread:
            loop_exit = True
        else:
            print("!!! Correct values: from {} to {} !!!".format(0,
                                                                 max_thread))
    # process all files using the selected number of threads
    pool = mp.Pool(n_threads)
    pool.map(labelling_func, CSV1_FILELIST)
    logging.info("----Program finished----")


if __name__ == '__main__':
    main()