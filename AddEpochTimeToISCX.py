import pandas as pd
from datetime import datetime
from datetime import timedelta
import copy

TZ_UTC_PLUS_ONE = 1
TZ_UTC_MINUS_FOUR = -4
TZ_DIFF = TZ_UTC_PLUS_ONE - TZ_UTC_MINUS_FOUR
NO_TZ_DIFF = 0


def convert_time(df_in, label_name, datetime_format, tz_diff, first_digit_hour, second_digit_hour):
    """
    Convert time string to epoch value

    Parameters
    ----------
    df_in: DataFrame
        Pandas DataFrame containing data
    label_name: str
        name of timestamp label
    datetime_format: str
        String describing date and time format
    tz_diff: int
        timezone difference

    Returns
    -------
    df_out

    """
    t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("[INFO] {} - Converting time strings to epoch ...".format(t))
    df_out = copy.deepcopy(df_in)
    epoch_origin = datetime(day=1, month=1, year=1970)
    n_rows = df_in.shape[0]
    period = int(n_rows / 10000)
    DataList = df_in.values.tolist()
    for i in range(n_rows):
        time = datetime.strptime(df_in[label_name][i], datetime_format)
        if (DataList[i][6][second_digit_hour-1] == ':'):
            hour = int(DataList[i][6][first_digit_hour])
            if (hour < 8):
                time_epoch = time + timedelta(hours=tz_diff + 12) - epoch_origin
            else:
                time_epoch = time + timedelta(hours=tz_diff) - epoch_origin
        else:
            hour = int(DataList[i][6][first_digit_hour:second_digit_hour])
            if (hour < 8):
                time_epoch = time + timedelta(hours=tz_diff + 12) - epoch_origin
            else:
                time_epoch = time + timedelta(hours=tz_diff) - epoch_origin
        time_seconds = time_epoch.total_seconds()
        df_out[label_name][i] = int(time_seconds)
        if i % period == 0:
            t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            print("\t{} - {}/{}".format(t, i, n_rows))
    return df_out


def monday_processing():
     csv_cic = "Monday-WorkingHours.pcap_ISCX.csv"
     df_cic = pd.read_csv(csv_cic)
     cic_date_format = '%d/%m/%Y %H:%M:%S'
     cic_label_name = ' Timestamp'
     first_digit_hour = 11
     second_digit_hour = 13
     df_cic = convert_time(df_cic, cic_label_name, cic_date_format, TZ_DIFF, first_digit_hour, second_digit_hour)
     df_cic.to_csv(r'Monday-WorkingHours-Epoch.pcap_ISCX.csv', index=False)


def tuesday_processing():
    csv_cic = "Tuesday-WorkingHours.pcap_ISCX.csv"
    df_cic = pd.read_csv(csv_cic)
    cic_date_format = '%d/%m/%Y %H:%M'
    cic_label_name = ' Timestamp'
    first_digit_hour = 9
    second_digit_hour = 11
    df_cic = convert_time(df_cic, cic_label_name, cic_date_format, TZ_DIFF, first_digit_hour, second_digit_hour)
    df_cic.to_csv(r'Tuesday-WorkingHours-Epoch.pcap_ISCX.csv', index=False)


def main():
    monday_processing()
    tuesday_processing()


if __name__ == "__main__":
    main()






